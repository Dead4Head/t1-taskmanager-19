package ru.t1.amsmirnov.taskmanager.service;

import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IService;
import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findOneById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) throws AbstractException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M remove(final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) throws AbstractException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public Boolean existById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(id);
    }

}
