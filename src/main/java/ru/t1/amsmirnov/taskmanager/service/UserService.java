package ru.t1.amsmirnov.taskmanager.service;

import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.RoleEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public User findOneByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @Override
    public Boolean isLoginExist(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExist(email);
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (isEmailExist(email)) throw new EmailExistException();
        final User user = this.create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return repository.remove(user);
    }

    @Override
    public User removeByEmail(final String email) throws AbstractException {
        final User user = findOneByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUserById(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws AbstractException {
        User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
