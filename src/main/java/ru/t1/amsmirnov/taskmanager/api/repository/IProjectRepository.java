package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectRepository extends IRepository<Project> {

}
