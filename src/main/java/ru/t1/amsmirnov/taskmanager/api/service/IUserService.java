package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IUserService extends IService<User>, IUserRepository {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User updateUserById(String id, String firstName, String lastName, String middleName) throws AbstractException;

}
