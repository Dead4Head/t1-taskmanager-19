package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task>, ITaskRepository {

    Task create(String name, String description) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task changeStatusById(String id, Status status) throws AbstractException;

    Task changeStatusByIndex(Integer index, Status status) throws AbstractException;

}
