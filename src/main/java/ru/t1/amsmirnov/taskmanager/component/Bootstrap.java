package ru.t1.amsmirnov.taskmanager.component;

import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.command.project.*;
import ru.t1.amsmirnov.taskmanager.command.system.*;
import ru.t1.amsmirnov.taskmanager.command.task.*;
import ru.t1.amsmirnov.taskmanager.command.user.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.amsmirnov.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;
import ru.t1.amsmirnov.taskmanager.service.*;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());

        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskClearCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());

        registry(new ApplicationExitCommand());
    }

    private void initDemoData() {
        try {
            projectService.add(new Project("Project 1", "Description 1", Status.IN_PROGRESS));
            projectService.create("Project 2", "Description 2");
            projectService.add(new Project("Project 3", "Description 3", Status.COMPLETED));

            taskService.create("TASK 1", "TASK 1");
            taskService.create("TASK 2", "TASK 2");

            userService.create("admin", "admin", Role.ADMIN);
            userService.create("test", "test", "test@test.test");
            userService.create("user", "user");
        } catch (Exception exception) {
            renderError(exception);
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    private void renderError(Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start(String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception exception) {
                renderError(exception);
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        getCommandService().add(command);
    }

    public boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        try {
            processArgument(argument);
        } catch (Exception exception) {
            renderError(exception);
        }
        return true;
    }

    public void processArgument(final String argument) throws AbstractException {
        final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    public void processCommand(final String commandName) throws AbstractException {
        final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        command.execute();
    }

    public void exit() {
        System.exit(0);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
