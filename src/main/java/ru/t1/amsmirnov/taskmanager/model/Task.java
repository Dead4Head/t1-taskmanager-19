package ru.t1.amsmirnov.taskmanager.model;

import ru.t1.amsmirnov.taskmanager.api.model.IWBS;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractModel implements IWBS {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private String projectId;
    private Date created = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

}
