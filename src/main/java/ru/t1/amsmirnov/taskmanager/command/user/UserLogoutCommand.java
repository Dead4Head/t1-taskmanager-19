package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";
    public static final String DESCRIPTION = "Logout from Task Manager.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
