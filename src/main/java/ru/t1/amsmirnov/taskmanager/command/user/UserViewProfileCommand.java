package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-view-profile";
    public static final String DESCRIPTION = "Shows user profile.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER PROFILE]");
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}
