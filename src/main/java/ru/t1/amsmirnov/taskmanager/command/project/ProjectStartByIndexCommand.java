package ru.t1.amsmirnov.taskmanager.command.project;

import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-index";
    public static final String DESCRIPTION = "Start project by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeStatusByIndex(index, Status.IN_PROGRESS);
    }

}
