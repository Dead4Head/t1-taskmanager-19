package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove-by-login";
    public static final String DESCRIPTION = "Remove user by login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.removeByLogin(login);
    }

}
