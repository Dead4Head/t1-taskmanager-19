package ru.t1.amsmirnov.taskmanager.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";
    public static final String DESCRIPTION = "Show developer info.";
    public static final String ARGUMENT = "-a";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Smirnov Anton");
        System.out.println("email: amsmirnov@t1-consulting.ru");
    }

}
