package ru.t1.amsmirnov.taskmanager.command.system;

import ru.t1.amsmirnov.taskmanager.api.service.ICommandService;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

      public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
