package ru.t1.amsmirnov.taskmanager.command.system;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";
    public static final String DESCRIPTION = "Close Task Manager.";
    public static final String ARGUMENT = null;

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
