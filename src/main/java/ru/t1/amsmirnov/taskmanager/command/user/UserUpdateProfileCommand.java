package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update-profile";
    public static final String DESCRIPTION = "Update current user's profile.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter first name: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUserById(
                userId, firstName, lastName, middleName
        );
    }

}
