package ru.t1.amsmirnov.taskmanager.repository;

import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
